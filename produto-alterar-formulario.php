<?php
require_once("dao-categoria.php");
require_once("dao-produto.php");
require_once("cabecalho.php"); 
$id =  $_POST["id"];
$categorias = listarCategorias($conexao);
$produto = pegaProduto($conexao, $id);
$usado = $produto['usado'] ? "checked='checked'" : "";
?>

<h1>Formulário de Alteração</h1>
<form action="altera-prouto.php" method="post">
    <input value="<?=$produto['id']?>" name="id" type="hidden">
       
        <?= include("formulario-base.php")?>
        
        <tr>
            <td><button class="btn btn-primary" type="submit">Alterar</button></td>
        </tr>
    </table>
</form>

<?php include("rodape.php"); ?>
