<?php
session_start();
function usuarioEstaLogado(){
    return isset($_SESSION["usuarioLogado"]);
}
function verificaUsuario(){
    
    if(!usuarioEstaLogado()){
        $_SESSION["danger"] = "Você não tem Acesso a Essa Funcionalidade!";
        header("Location: index.php");
        die();
    }
}

function usuarioLogado(){
    return $_SESSION["usuarioLogado"];
}

function logarUsuario($email){
    $_SESSION["usuarioLogado"] = $email;
}

function logout(){
    session_destroy();
    session_start();
}
