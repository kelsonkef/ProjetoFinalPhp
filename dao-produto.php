<?php
require_once("conexao.php");

function insereProduto($conexao, $nome, $preco, $descricao, $categoria_id, $usado) {
    $nome = mysqli_real_escape_string($conexao, $nome);
    $descricao = mysqli_real_escape_string($conexao, $descricao);
    $query = "insert into produtos (nome, preco, descricao, categoria_id, usado) values ('{$nome}', '{$preco}', '{$descricao}', '{$categoria_id}', '{$usado}')";
    return mysqli_query($conexao, $query);
}
function listarProduto($conexao){
    $vetor = array();
    $query = "select p.*, c.nome as categoria_nome from produtos as p join categorias as c on p.categoria_id = c.id";
    $resultado = mysqli_query($conexao, $query);
    while($produto = mysqli_fetch_assoc($resultado)){
        array_push($vetor, $produto);
    }
    return $vetor;
}
function deletarProduto($conexao, $id){
    $query =  "DELETE FROM produtos WHERE id='{$id}'";
    return mysqli_query($conexao, $query);
}
function pegaProduto($conexao, $id){
    $query = "SELECT * FROM produtos WHERE id='{$id}' ";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}
function alteraProduto($conexao, $id, $nome, $preco, $descricao, $categoria_id, $usado){
    $nome = mysqli_real_escape_string($conexao, $nome);
    $descricao = mysqli_real_escape_string($conexao, $descricao);
    $query = "UPDATE produtos set nome = '{$nome}', preco = '{$preco}', descricao =  '{$descricao}', categoria_id = '{$categoria_id}', usado = '{$usado}' WHERE id = '{$id}'";
    return mysqli_query($conexao, $query);
}
